#!/bin/bash

# move to dir we are installing in
cd /usr/src

#installJRE
yum install java-1.7.0-openjdk -y

# Install RubyGems
# wget http://production.cf.rubygems.org/rubygems/rubygems-1.8.25.tgz
# tar xvzf rubygems-1.8.25.tgz
# cd rubygems-1.8.25
# ruby setup.rb config
# ruby setup.rb setup
# ruby setup.rb install
# cd ..

# init in the FE web root
cd /vagrant/htdocs/pivot-2/

# FE config
# mv config.json.tmp config.json
# export NODE_PATH=/opt/lib/node_modules

# Install Bower and Grunt globally
npm install -g grunt-cli
npm install -g bower

# Install sass
gem install sass

# npm install grunt-notify --save-dev
# npm install pako
# npm install handlebars

# Install NPM and run grunt
npm install --no-bin-links
grunt setup
# grunt

# init in the API web root
cd /vagrant/htdocs/pivot-apigility/

chmod 777 /var/lib/php/session/
chmod 777 /tmp

php tests/pivot-tests.php database:create
php tests/pivot-tests.php database:fresh

php public/index.php development enable

./composer install
